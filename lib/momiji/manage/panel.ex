defmodule Momiji.Manage.Panel do
  use Ecto.Schema
  import Ecto.Changeset


  schema "guilds" do
    field :has_momiji, :boolean
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(panel, attrs) do
    panel
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
