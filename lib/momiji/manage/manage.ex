defmodule Momiji.Manage do
  @moduledoc """
  The Manage context.
  """

  import Ecto.Query, warn: false
  alias Momiji.Repo

  alias Momiji.Manage.Panel

  @doc """
  Returns the list of manage.

  ## Examples

      iex> list_manage()
      [%Panel{}, ...]

  """
  def list_manage do
    Repo.all(Panel)
  end

  @doc """
  Gets a single panel.

  Raises `Ecto.NoResultsError` if the Panel does not exist.

  ## Examples

      iex> get_panel!(123)
      %Panel{}

      iex> get_panel!(456)
      ** (Ecto.NoResultsError)

  """
  def get_panel!(id), do: Repo.get!(Panel, id)

  @doc """
  Creates a panel.

  ## Examples

      iex> create_panel(%{field: value})
      {:ok, %Panel{}}

      iex> create_panel(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_panel(attrs \\ %{}) do
    %Panel{}
    |> Panel.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a panel.

  ## Examples

      iex> update_panel(panel, %{field: new_value})
      {:ok, %Panel{}}

      iex> update_panel(panel, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_panel(%Panel{} = panel, attrs) do
    panel
    |> Panel.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Panel.

  ## Examples

      iex> delete_panel(panel)
      {:ok, %Panel{}}

      iex> delete_panel(panel)
      {:error, %Ecto.Changeset{}}

  """
  def delete_panel(%Panel{} = panel) do
    Repo.delete(panel)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking panel changes.

  ## Examples

      iex> change_panel(panel)
      %Ecto.Changeset{source: %Panel{}}

  """
  def change_panel(%Panel{} = panel) do
    Panel.changeset(panel, %{})
  end
end
