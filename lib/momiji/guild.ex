defmodule Momiji.Guild do
  use Ecto.Schema
  import Ecto.Changeset


  schema "guilds" do
    field :has_momiji, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(guild, attrs) do
    guild
    |> cast(attrs, [:has_momiji])
    |> validate_required([:has_momiji])
  end
end
