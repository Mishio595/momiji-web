defmodule MomijiWeb.PanelController do
  use MomijiWeb, :controller

  alias Momiji.Manage
  alias Momiji.Manage.Panel

  def index(conn, _params) do
    manage = Manage.list_manage()
    render(conn, "index.html", manage: manage)
  end

  def new(conn, _params) do
    changeset = Manage.change_panel(%Panel{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"panel" => panel_params}) do
    case Manage.create_panel(panel_params) do
      {:ok, panel} ->
        conn
        |> put_flash(:info, "Panel created successfully.")
        |> redirect(to: panel_path(conn, :show, panel))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    panel = Manage.get_panel!(id)
    render(conn, "show.html", panel: panel)
  end

  def edit(conn, %{"id" => id}) do
    panel = Manage.get_panel!(id)
    changeset = Manage.change_panel(panel)
    render(conn, "edit.html", panel: panel, changeset: changeset)
  end

  def update(conn, %{"id" => id, "panel" => panel_params}) do
    panel = Manage.get_panel!(id)

    case Manage.update_panel(panel, panel_params) do
      {:ok, panel} ->
        conn
        |> put_flash(:info, "Panel updated successfully.")
        |> redirect(to: panel_path(conn, :show, panel))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", panel: panel, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    panel = Manage.get_panel!(id)
    {:ok, _panel} = Manage.delete_panel(panel)

    conn
    |> put_flash(:info, "Panel deleted successfully.")
    |> redirect(to: panel_path(conn, :index))
  end
end
