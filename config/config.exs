# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :momiji,
  ecto_repos: [Momiji.Repo]

# Configures the endpoint
config :momiji, MomijiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "tQ8QbaKe1TOLuFYWwy8QvdiRWE6fOMQFaMsOc0f3xhlutxJak8PqjLzeu1bbAt/g",
  render_errors: [view: MomijiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Momiji.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
