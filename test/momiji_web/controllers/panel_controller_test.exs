defmodule MomijiWeb.PanelControllerTest do
  use MomijiWeb.ConnCase

  alias Momiji.Manage

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:panel) do
    {:ok, panel} = Manage.create_panel(@create_attrs)
    panel
  end

  describe "index" do
    test "lists all manage", %{conn: conn} do
      conn = get conn, panel_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Manage"
    end
  end

  describe "new panel" do
    test "renders form", %{conn: conn} do
      conn = get conn, panel_path(conn, :new)
      assert html_response(conn, 200) =~ "New Panel"
    end
  end

  describe "create panel" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, panel_path(conn, :create), panel: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == panel_path(conn, :show, id)

      conn = get conn, panel_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Panel"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, panel_path(conn, :create), panel: @invalid_attrs
      assert html_response(conn, 200) =~ "New Panel"
    end
  end

  describe "edit panel" do
    setup [:create_panel]

    test "renders form for editing chosen panel", %{conn: conn, panel: panel} do
      conn = get conn, panel_path(conn, :edit, panel)
      assert html_response(conn, 200) =~ "Edit Panel"
    end
  end

  describe "update panel" do
    setup [:create_panel]

    test "redirects when data is valid", %{conn: conn, panel: panel} do
      conn = put conn, panel_path(conn, :update, panel), panel: @update_attrs
      assert redirected_to(conn) == panel_path(conn, :show, panel)

      conn = get conn, panel_path(conn, :show, panel)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, panel: panel} do
      conn = put conn, panel_path(conn, :update, panel), panel: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Panel"
    end
  end

  describe "delete panel" do
    setup [:create_panel]

    test "deletes chosen panel", %{conn: conn, panel: panel} do
      conn = delete conn, panel_path(conn, :delete, panel)
      assert redirected_to(conn) == panel_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, panel_path(conn, :show, panel)
      end
    end
  end

  defp create_panel(_) do
    panel = fixture(:panel)
    {:ok, panel: panel}
  end
end
