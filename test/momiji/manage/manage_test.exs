defmodule Momiji.ManageTest do
  use Momiji.DataCase

  alias Momiji.Manage

  describe "manage" do
    alias Momiji.Manage.Panel

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def panel_fixture(attrs \\ %{}) do
      {:ok, panel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Manage.create_panel()

      panel
    end

    test "list_manage/0 returns all manage" do
      panel = panel_fixture()
      assert Manage.list_manage() == [panel]
    end

    test "get_panel!/1 returns the panel with given id" do
      panel = panel_fixture()
      assert Manage.get_panel!(panel.id) == panel
    end

    test "create_panel/1 with valid data creates a panel" do
      assert {:ok, %Panel{} = panel} = Manage.create_panel(@valid_attrs)
    end

    test "create_panel/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Manage.create_panel(@invalid_attrs)
    end

    test "update_panel/2 with valid data updates the panel" do
      panel = panel_fixture()
      assert {:ok, panel} = Manage.update_panel(panel, @update_attrs)
      assert %Panel{} = panel
    end

    test "update_panel/2 with invalid data returns error changeset" do
      panel = panel_fixture()
      assert {:error, %Ecto.Changeset{}} = Manage.update_panel(panel, @invalid_attrs)
      assert panel == Manage.get_panel!(panel.id)
    end

    test "delete_panel/1 deletes the panel" do
      panel = panel_fixture()
      assert {:ok, %Panel{}} = Manage.delete_panel(panel)
      assert_raise Ecto.NoResultsError, fn -> Manage.get_panel!(panel.id) end
    end

    test "change_panel/1 returns a panel changeset" do
      panel = panel_fixture()
      assert %Ecto.Changeset{} = Manage.change_panel(panel)
    end
  end
end
